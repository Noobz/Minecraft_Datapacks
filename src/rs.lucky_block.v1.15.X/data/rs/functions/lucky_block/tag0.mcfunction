tag @a remove actions.damage0
execute as @a[scores={damage0=1..}] run function rs:lucky_block/players/events/damage0

execute as @a[scores={sn.glass_yellow=1..}] run function rs:lucky_block/players/events/sn.glass_yellow
execute as @a[scores={sn.cook_chicken=1..}] run function rs:lucky_block/players/events/sn.cooked_chicken

execute as @e[type=armor_stand,nbt={Invulnerable:1b,Marker:1b}] run data modify entity @s Fire set value 32767s