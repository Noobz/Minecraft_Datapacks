scoreboard objectives add damage0 minecraft.custom:damage_dealt

scoreboard objectives add sn.cook_chicken minecraft.used:minecraft.cooked_chicken
scoreboard objectives add sn.glass_yellow minecraft.used:minecraft.yellow_stained_glass

scoreboard objectives add de.stone minecraft.mined:minecraft.stone

scoreboard objectives add tricked_age dummy
scoreboard objectives add luck_level dummy