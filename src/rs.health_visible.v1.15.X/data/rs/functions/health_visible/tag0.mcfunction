tag @e remove entity.kind.mob
tag @e remove entity.kind.obj
execute as @e[type=!armor_stand] if data entity @s HurtTime run tag @s add entity.kind.mob
execute as @e[tag=!entity.kind.mob] run tag @s add entity.kind.obj

scoreboard players add time.tick.24000 hv.time 1
execute if score time.tick.24000 hv.time matches 24000.. run scoreboard players set time.tick.24000 hv.time 0
scoreboard players operation time.tick.2 hv.time = time.tick.24000 hv.time
scoreboard players operation time.tick.2 hv.time %= #2 operations

tag @e remove invisible0
tag @e[nbt={ActiveEffects:[{Id:14b}]}] add invisible0