execute as @e[type=item,nbt={Item:{id:"minecraft:yellow_dye",Count:1b}}] at @s as @e[type=item,nbt={Item:{id:"minecraft:iron_ingot",Count:4b}},limit=1,distance=..1] at @s run function rs:lucky_block/system/ways/props/craf.lucky_block

execute as @e[tag=as.lucky_block.0] at @s unless block ~ ~2 ~ minecraft:yellow_stained_glass positioned ~ ~1.2 ~ run function rs:lucky_block/system/ways/props/dest.lucky_block
execute as @e[tag=as.lucky_block.0] run data modify entity @s Fire set value 32767s

tag @a remove se.lucky_block
tag @a[nbt={SelectedItem:{tag:{rsid:"lucky_block"}}}] add se.lucky_block

tag @a remove se.golden_chicken
tag @a[nbt={SelectedItem:{tag:{rsid:"golden_chicken"}}}] add se.golden_chicken

execute as @e[tag=tricked.tnt,nbt={Fuse:1s}] at @s run summon chicken ~ ~ ~
kill @e[tag=tricked.tnt,nbt={Fuse:1s}]

execute as @a[tag=actions.damage0,nbt={SelectedItem:{tag:{rsid:"lucky_sword"}}}] at @s run effect give @e[nbt={HurtTime:10s},limit=1,sort=nearest] levitation 4 1 true
scoreboard players add @e[type=item,nbt={Item:{tag:{tricked.diamonds:1b}}}] tricked_age 1
execute as @e[type=item,nbt={Item:{tag:{tricked.diamonds:1b}}}] run data modify entity @s PickupDelay set value 32767s
kill @e[type=item,scores={tricked_age=60..}]

scoreboard players reset @a luck_level
scoreboard players add @a[gamemode=!spectator,nbt={Inventory:[{Slot:103b,tag:{rsid:"lucky_helmet"}}]}] luck_level 1
scoreboard players add @a[gamemode=!spectator,nbt={Inventory:[{Slot:102b,tag:{rsid:"lucky_chestplate"}}]}] luck_level 1
scoreboard players add @a[gamemode=!spectator,nbt={Inventory:[{Slot:101b,tag:{rsid:"lucky_leggings"}}]}] luck_level 1
scoreboard players add @a[gamemode=!spectator,nbt={Inventory:[{Slot:100b,tag:{rsid:"lucky_boots"}}]}] luck_level 1
effect clear @a[scores={luck_level=1..}] luck
effect give @a[scores={luck_level=1}] luck 1 0 true
effect give @a[scores={luck_level=2}] luck 1 1 true
effect give @a[scores={luck_level=3}] luck 1 2 true
effect give @a[scores={luck_level=4}] luck 1 3 true