tag @e remove this0
tag @s add this0

execute as @s[type=item] if data entity @s CustomName run tag @s add hv.item_names
execute as @s[type=item,tag=hv.item_names] run data modify storage rs:rs hv.item_names set value ''
execute as @s[type=item,tag=hv.item_names] run data modify storage rs:rs hv.item_names set from entity @s CustomName
execute as @s[type=item,tag=hv.item_names] run data modify entity @s CustomName set value ''
execute as @s[type=player] store result score fole health run data get entity @s foodLevel
execute as @s[type=player] store result score xple health run data get entity @s XpLevel
execute as @s[type=!item] store result score nowhealth health run data get entity @s Health
execute as @s[type=!item] store result score maxhealth health run attribute @s minecraft:generic.max_health get
execute as @s[type=item] store result score nowhealth health run data get entity @s Item.Count
execute as @s[type=item] store success score itna health run data get entity @s Item.tag.display.Name
execute as @s[type=iron_golem] store result score plcr health run data get entity @s PlayerCreated
execute as @s[type=wolf] store success score owner health run data get entity @s Owner
execute as @s[type=wolf] if score owner health matches 1 if data entity @s ArmorItems[-1].tag.owner_name run scoreboard players set owner health 2

execute as @s in overworld run data modify block 103103 157 103103 Text1 set value '{"text":""}'
execute as @s[type=player] in overworld run data modify block 103103 157 103103 Text1 set value '[{"text":"§r§c[§6✎§c]§r §c[§eLv§r"},{"score":{"objective":"health","name":"xple"},"color":"gold"},{"text":"§r§c]§r §6§l玩家§r "},{"score":{"objective":"health","name":"nowhealth"},"color":"green"},{"text":"§r§7/§r"},{"score":{"objective":"health","name":"maxhealth"},"color":"aqua"},{"text":"§r§c❤§r §f[§r"},{"score":{"objective":"health","name":"fole"},"color":"aqua"},{"text":"§r§6☕§f]§r"}]'
execute as @s[type=!player,type=!item,type=!iron_golem,type=!wolf] in overworld run data modify block 103103 157 103103 Text1 set value '[{"text":"§r§c[§6✎§c]§r "},{"selector":"@e[tag=this0,limit=1]","bold":"true","color":"yellow"},{"text":"§r "},{"score":{"objective":"health","name":"nowhealth"},"color":"green"},{"text":"§r§7/§r"},{"score":{"objective":"health","name":"maxhealth"},"color":"aqua"},{"text":"§r§c❤§r"}]'
execute as @s[type=item] if score itna health matches 0 in overworld run data modify block 103103 157 103103 Text1 set value '[{"text":"§r§c[§6✎§c]§r "},{"selector":"@e[tag=this0,limit=1]","color":"green"},{"text":"§r§7x§r"},{"score":{"objective":"health","name":"nowhealth"},"color":"aqua"}]'
execute as @s[type=item] if score itna health matches 1 at @s run summon area_effect_cloud ~ ~ ~ {Tags:["itna"],Duration:2}
execute as @s[type=item] if score itna health matches 1 at @s run data modify entity @e[tag=itna,limit=1] CustomName set from entity @e[tag=this0,limit=1] Item.tag.display.Name
execute as @s[type=item] if score itna health matches 1 in overworld run data modify block 103103 157 103103 Text1 set value '[{"text":"§r§c[§6✎§c]§r "},{"selector":"@e[tag=itna,limit=1]","color":"light_purple","italic":"true"},{"text":"§r§7x§r"},{"score":{"objective":"health","name":"nowhealth"},"color":"aqua"}]'
tag @e remove itna

execute as @s[type=iron_golem] if score plcr health matches 0 in overworld run data modify block 103103 157 103103 Text1 set value '[{"text":"§r§c[§6✎§c]§r §r§2[§a村庄建造§2]§r "},{"selector":"@e[tag=this0,limit=1]","bold":"true","color":"yellow"},{"text":"§r "},{"score":{"objective":"health","name":"nowhealth"},"color":"green"},{"text":"§r§7/§r"},{"score":{"objective":"health","name":"maxhealth"},"color":"aqua"},{"text":"§r§c❤§r"}]'
execute as @s[type=iron_golem] if score plcr health matches 1 in overworld run data modify block 103103 157 103103 Text1 set value '[{"text":"§r§c[§6✎§c]§r §r§4[§c玩家建造§4]§r "},{"selector":"@e[tag=this0,limit=1]","bold":"true","color":"yellow"},{"text":"§r "},{"score":{"objective":"health","name":"nowhealth"},"color":"green"},{"text":"§r§7/§r"},{"score":{"objective":"health","name":"maxhealth"},"color":"aqua"},{"text":"§r§c❤§r"}]'
execute as @s[type=wolf] if score owner health matches 0 in overworld run data modify block 103103 157 103103 Text1 set value '[{"text":"§r§c[§6✎§c]§r §r§a[§e野生§a]§r "},{"selector":"@e[tag=this0,limit=1]","bold":"true"},{"text":"§r "},{"score":{"objective":"health","name":"nowhealth"},"color":"green"},{"text":"§r§7/§r"},{"score":{"objective":"health","name":"maxhealth"},"color":"aqua"},{"text":"§r§c❤§r"}]'
execute as @s[type=wolf] if score owner health matches 1 in overworld run data modify block 103103 157 103103 Text1 set value '[{"text":"§r§c[§6✎§c]§r §r§9[§c未知主人§9]§r "},{"selector":"@e[tag=this0,limit=1]","bold":"true"},{"text":"§r "},{"score":{"objective":"health","name":"nowhealth"},"color":"green"},{"text":"§r§7/§r"},{"score":{"objective":"health","name":"maxhealth"},"color":"aqua"},{"text":"§r§c❤§r"}]'
execute as @s[type=wolf] if score owner health matches 2 in overworld run data modify block 103103 157 103103 Text1 set value '[{"text":"§r§c[§6✎§c]§r "},{"nbt":"ArmorItems[-1].tag.owner_name","entity":"@e[tag=this0,limit=1]","color":"yellow"},{"text":"§r§e的§r"},{"selector":"@e[tag=this0,limit=1]","color":"yellow"},{"text":"§r "},{"score":{"objective":"health","name":"nowhealth"},"color":"green"},{"text":"§r§7/§r"},{"score":{"objective":"health","name":"maxhealth"},"color":"aqua"},{"text":"§r§c❤§r"}]'

execute at @s anchored eyes rotated 0 0 run summon minecraft:area_effect_cloud ^ ^0.19 ^ {Tags:["aec.health_visible","data.not"],CustomNameVisible:1b,Invulnerable:1b,NoGravity:1b,Duration:2,Fire:200s}
execute as @e[tag=data.not,limit=1] in minecraft:overworld run data modify entity @s CustomName set from block 103103 157 103103 Text1
tag @e remove data.not

execute as @s[type=item,tag=hv.item_names] run data modify entity @s CustomName set from storage rs:rs hv.item_names
tag @s remove hv.item_names

tag @s remove this0