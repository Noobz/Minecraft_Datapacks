# **Minecraft_Datapacks**
Hello, I've made two datapacks\
You can use them to use or study\
\
**xtidozy' site:[rune.my.to](https://rune.my.to)**
## 下载:Download
### 血量显示:health_visible
- ***[rs.health_visible.v1.15.X.zip](https://gitlab.com/Noobz/Minecraft_Datapacks/-/raw/main/assets/rs.health_visible.v1.15.X.zip?inline=false)***
- ***[rs.health_visible.v1.16.X.zip](https://gitlab.com/Noobz/Minecraft_Datapacks/-/raw/main/assets/rs.health_visible.v1.16.X.zip?inline=false)***
### 幸运方块:lucky_block
- ***[rs.lucky_block.v1.15.X.zip](https://gitlab.com/Noobz/Minecraft_Datapacks/-/raw/main/assets/rs.lucky_block.v1.15.X.zip?inline=false)***
- ***[rs.lucky_block.v1.16.X.zip](https://gitlab.com/Noobz/Minecraft_Datapacks/-/raw/main/assets/rs.lucky_block.v1.16.X.zip?inline=false)***
## 解锁新内容:Unlocked New Contents
### 幸运方块:lucky_block
#### Minecraft 1.15.X and Minecraft 1.16.X:
- **你可以通过在地面上丢四个铁锭和一个黄色染料来合成幸运方块\
You can make lucky blocks by dropping four
iron ingots and a yellow dye on the ground**
- **放置幸运方块后将其打碎 一些不会破坏地形的随机事件将会发生\
Place the lucky blocks and break them, random
events that don't destroy the terrain will happen**
### 血量显示:health_visible
#### Minecraft 1.15.X:
- **它可以显示生物名字和血量 不和生物自定义名字产生冲突\
It can display creature name and health and it
does not conflict with the creature custom name**
- **被命名后的生物优先显示自定义的名字\
Named creatures first display their custom name**
- **隐身生物和旁观者不会显示血量\
Invisible creatures and spectators will not show their health**
- **血量显示文字会出现在生物的头部上方\
The health display text
will appear above the head of the creature**
- **也可以显示掉落物的物品名字和数量\
It will also display the
name and count of items**
- **不和掉落物实体自定义名字产生冲突\
It will not conflict with
the custom name of the item**
- **会显示物品的名字 优先显示物品的自定义名字\
It will display
the name of the item, the custom name of the item is displayed first**
- **显示物品的自定义名字时 字体会变成紫色斜体\
When the custom
name of the item is displayed, the font becomes purple italic**
- **在铁傀儡的头上 会显示它是 [村庄建造] 或 [玩家建造]\
On the head of the iron golems, it shows that
it is either [village build] or [player build]**
#### Minecraft 1.16.X:
- **在基础上 新添加了狼的主人信息显示\
Besides, it newly
added wolf owner information display**
- **在主人在线并且狼已被加载时 狼就会储存主人的名字\
When the owner is online and the wolf
has been loaded, the wolf will store
the owner's name**
- **储存了名字的狼会显示自己的主人的名字\
The wolf that
stores the owner name will
display its owner's name**
- **否则显示 [未知主人] 或 [野生]\
Otherwise it display [unknown owner] or [wild]**
---
![cedo](assets/portable/_.png)
---
![cedo](assets/portable/00.png)
---
![cedo](assets/portable/01.png)
---
![cedo](assets/portable/02.png)
---
![cedo](assets/portable/03.png)
---
![cedo](assets/portable/04.png)
---
![cedo](assets/portable/05.png)
---
![cedo](assets/portable/06.png)
---
![cedo](assets/portable/07.png)
---
![cedo](assets/portable/08.png)
---